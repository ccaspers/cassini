export default function Json({ data }: { data: unknown }) {
  return <pre>{JSON.stringify(data, undefined, 2)}</pre>;
}
