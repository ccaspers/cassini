import { ChangeEvent, FormEvent, useState } from "react";

interface Props {
  className?: string;
  onSearch?: (value: string) => void;
  slim?: boolean;
  initialQuery?: string;
}

export default function SearchBox({
  className,
  onSearch,
  slim,
  initialQuery = "",
}: Props) {
  const [query, setQuery] = useState(initialQuery);

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    event.stopPropagation();
    onSearch?.(query);
  };

  const updateState = (e: ChangeEvent<HTMLInputElement>) => {
    setQuery(e.target.value);
  };

  return (
    <form className={className} onSubmit={handleSubmit}>
      <div
        className={`flex w-full rounded-full border border-gray-400 bg-white 
        opacity-50 transition-all 
        focus-within:border-teal-300
        focus-within:opacity-100 focus-within:ring-4
        focus-within:ring-teal-100`}
      >
        <input
          type="text"
          id="search"
          className={`w-full border-none bg-transparent focus:outline-none ${
            slim ? "px-4 py-3" : "p-4"
          }`}
          value={query}
          onChange={updateState}
        />
        <button
          type="submit"
          className={`group rounded-full outline-none ring-teal-200  transition-all focus:ring-2 ${
            slim ? "p-3" : "p-4"
          }`}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={1.5}
            stroke="currentColor"
            className="h-6 w-6 transition-all hover:stroke-teal-500 group-focus:stroke-teal-500"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z"
            />
          </svg>
        </button>
      </div>
    </form>
  );
}
