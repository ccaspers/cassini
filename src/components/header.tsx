import Brand from "./brand";
import SearchBox from "./search-box";

type Props = {
  onSearch?: (value: string) => void;
  className?: string;
  initialQuery?: string;
};

export default function Header({
  onSearch,
  className = "",
  initialQuery = "",
}: Props) {
  return (
    <div
      className={`flex items-center justify-between border-b border-dotted border-gray-400 pb-4 pl-3 ${className}`}
    >
      <Brand slim align="left" />
      <SearchBox
        className="w-1/3"
        initialQuery={initialQuery}
        onSearch={onSearch}
        slim
      />
    </div>
  );
}
