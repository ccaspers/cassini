import { Issue } from "@octokit/graphql-schema";
import Link from "next/link";

type Props = {
  className?: string;
  issue: Issue;
};

function IssueListItem({ issue }: Props) {
  return (
    <li className="border-b border-dotted border-gray-400 bg-white p-3 text-sm odd:bg-gray-100">
      <Link href={`/issues/${issue.number}`}>{issue.title}</Link>
    </li>
  );
}

type ListProps = {
  className?: string;
  closedCount?: number;
  issues?: Issue[];
  openCount?: number;
};

export default function IssueList({ issues = [], className = "" }: ListProps) {
  const listItems = issues.map((issue, index) => {
    return <IssueListItem key={index} issue={issue} />;
  });

  return <ul className={className}>{listItems}</ul>;
}
