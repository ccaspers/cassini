type MessageType = "error" | "warning" | "info";
type Props = React.PropsWithChildren<{
  type?: MessageType;
}>;

const colors: Record<MessageType, string> = {
  error: "bg-red-200 border-red-500 text-red-700",
  warning: "bg-yellow-200 border-yellow-500 text-yellow-700",
  info: "bg-teal-200 border-teal-500 text-teal-700",
};

export default function Message({ type = "info", children }: Props) {
  return (
    <div className={`rounded-lg border p-3 ${colors[type]}`}>{children}</div>
  );
}
