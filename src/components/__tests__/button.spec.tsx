import Button from "@/components/button";
import { render, screen } from "@testing-library/react";

describe("The Button", () => {
  it("should call the click listener", () => {
    const callback = jest.fn();
    render(<Button onClick={callback}>Test Button</Button>);
    screen.getByText("Test Button").click();
    expect(callback).toHaveBeenCalledTimes(1);
  });
});
