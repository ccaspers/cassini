import Brand from "@/components/brand";
import { config } from "@/config";
import { render } from "@testing-library/react";

describe("The Brand Component", () => {
  it("it should use the config to render the link's title", () => {
    config.repoOwner = "owner";
    config.repoName = "repoName";
    const result = render(<Brand />);
    expect(result.container.innerHTML).toContain("owner/repo");
  });
});
