import Link from "next/link";
import { ParsedUrlQueryInput } from "querystring";

type Props = {
  active?: boolean;
  count?: number;
  label: string;
  pathname: string;
  query: ParsedUrlQueryInput;
  state: "open" | "closed";
};

export default function StateLink({
  active,
  count,
  label,
  pathname,
  query,
  state,
}: Props) {
  const renderedLabel = `${label} (${
    Number.isInteger(count) ? (count as number) : "?"
  })`;
  return (
    <Link
      className={`p-4 pt-2 font-mono text-xs ${active ? "font-bold" : ""}`}
      href={{
        pathname,
        query: {
          ...query,
          s: state,
        },
      }}
    >
      {renderedLabel}
    </Link>
  );
}
