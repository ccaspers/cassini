import { MouseEventHandler } from "react";

type Props = React.PropsWithChildren<{
  onClick?: MouseEventHandler<HTMLButtonElement>;
}>;
export default function Button({ children, onClick }: Props) {
  return (
    <button
      onClick={onClick}
      className="transition-color mt-2 border border-dotted border-gray-400 px-4 py-2 font-mono text-sm font-bold capitalize hover:border-teal-500 hover:text-teal-500"
    >
      {children}
    </button>
  );
}
