import { config } from "@/config";
import Link from "next/link";

type Props = {
  align?: "center" | "left";
  className?: string;
  slim?: boolean;
};

export default function Brand({
  align = "center",
  className = "",
  slim = false,
}: Props) {
  const repo = `${config.repoOwner}/${config.repoName}`;
  const repoUrl = `https://github.com/${repo}`;
  const textAlignment = align === "center" ? "text-center" : "text-left";

  return (
    <div className={`${textAlignment} ${className}`}>
      <h1
        className={`font-display ${
          slim ? "text-6xl" : "text-8xl"
        } text-teal-500`}
      >
        <Link href="/">Cassini</Link>
      </h1>
      <h2 className={`font-mono ${slim ? "text-md" : "text-xl"} italic`}>
        Search{" "}
        <Link href={repoUrl} className="font-bold underline decoration-dotted">
          {repo}
        </Link>{" "}
        Issues
      </h2>
    </div>
  );
}
