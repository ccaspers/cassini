type Props = {
  author?: string;
  className?: string;
  createdAt?: string;
  html?: string;
  lastEditedAt?: string;
};

export default function Comment({
  author = "",
  className = "",
  lastEditedAt,
  createdAt,
  html = "",
}: Props) {
  return (
    <div className={`${className} flex flex-col border`}>
      <div className="flex justify-between bg-gray-50 p-2 font-mono text-sm">
        <div>{author}</div>

        <div>{renderTimestamps({ createdAt, lastEditedAt })}</div>
      </div>
      <div className="comment p-2" dangerouslySetInnerHTML={{ __html: html }} />
    </div>
  );
}

function renderTimestamps({
  createdAt,
  lastEditedAt,
}: {
  createdAt?: string;
  lastEditedAt?: string;
}) {
  const output = [
    createdAt ? `Created At: ${createdAt}` : undefined,
    lastEditedAt ? `Last Edited: ${lastEditedAt}` : undefined,
  ].filter((s): s is string => !!s);

  return output.join(" | ");
}
