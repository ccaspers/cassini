type Props = {
  className: string;
};

export default function Footer({ className }: Props) {
  return (
    <footer
      className={`w-full border-t border-dotted border-gray-400 p-4 text-center font-mono text-sm font-bold ${className}`}
    >
      {" "}
      Copyright &copy; {new Date().getFullYear()} Christian Caspers
    </footer>
  );
}
