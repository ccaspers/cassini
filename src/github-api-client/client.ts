import { config } from "@/config";
import { ApolloClient, InMemoryCache } from "@apollo/client";
import { relayStylePagination } from "@apollo/client/utilities";
import { IssueCommentConnection } from "@octokit/graphql-schema";

export const client = new ApolloClient({
  uri: "https://api.github.com/graphql",
  cache: new InMemoryCache({
    typePolicies: {
      Query: {
        fields: {
          search: relayStylePagination(["query"]),
        },
      },
      Issue: {
        fields: {
          comments: {
            keyArgs: false,
            merge(
              existing: IssueCommentConnection | undefined,
              incoming: IssueCommentConnection
            ) {
              if (!existing) {
                return incoming;
              }

              return {
                ...existing,
                ...incoming,
                nodes: [existing.nodes, incoming.nodes]
                  .filter((nodes) => Array.isArray(nodes))
                  .flat(),
              };
            },
          },
        },
      },
    },
  }),
  headers: {
    Authorization: `Bearer ${config.token}`,
  },
});
