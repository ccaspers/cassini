export const config = {
  token: process.env.NEXT_PUBLIC_GITHUB_API_TOKEN || "",
  repoName: process.env.NEXT_PUBLIC_GITHUB_REPO_NAME || "",
  repoOwner: process.env.NEXT_PUBLIC_GITHUB_REPO_OWNER || "",
};
