import { gql } from "@apollo/client";

export const ISSUE_COUNT_QUERY = gql`
  query getIssueCount($query: String!) {
    search(type: ISSUE, query: $query) {
      issueCount
    }
  }
`;
