import { gql } from "@apollo/client";

export const ISSUE_QUERY = gql`
  query getIssue(
    $issueNumber: Int!
    $owner: String!
    $name: String!
    $cursor: String
  ) {
    repository(owner: $owner, name: $name) {
      issue(number: $issueNumber) {
        state
        title
        bodyHTML
        createdAt
        lastEditedAt
        author {
          login
        }
        comments(first: 5, after: $cursor) {
          totalCount
          pageInfo {
            hasNextPage
            endCursor
          }
          nodes {
            bodyHTML
            createdAt
            lastEditedAt
            author {
              login
            }
          }
        }
      }
    }
  }
`;
