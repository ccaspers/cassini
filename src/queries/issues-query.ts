import { gql } from "@apollo/client";

export const ISSUES_QUERY = gql`
  query searchIssues($query: String!, $cursor: String) {
    search(type: ISSUE, first: 10, query: $query, after: $cursor) {
      issueCount
      pageInfo {
        hasNextPage
        endCursor
      }
      edges {
        node {
          ... on Issue {
            title
            number
          }
        }
      }
    }
  }
`;
