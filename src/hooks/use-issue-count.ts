import { ISSUE_COUNT_QUERY } from "@/queries/issue-count-query";
import { useQuery } from "@apollo/client";
import { SearchResultItemConnection } from "@octokit/graphql-schema";

export default function useIssueCount(query: string) {
  const { data } = useQuery<{
    search: SearchResultItemConnection;
  }>(ISSUE_COUNT_QUERY, {
    variables: {
      query,
    },
  });
  return data?.search.issueCount;
}
