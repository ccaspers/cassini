import Brand from "@/components/brand";
import SearchBox from "@/components/search-box";
import BaseLayout from "@/layouts/base-layout";
import { useRouter } from "next/router";

export default function Home() {
  const router = useRouter();
  return (
    <BaseLayout className="min-h-[75%] w-full items-center justify-center">
      <Brand />
      <SearchBox
        className="mt-5 w-1/3"
        onSearch={(q) => {
          router
            .push({
              pathname: "/issues",
              query: {
                q,
              },
            })
            .catch(() => {
              // ignore errors
            });
        }}
      />
    </BaseLayout>
  );
}
