import { ISSUE_COUNT_QUERY } from "@/queries/issue-count-query";
import { ISSUES_QUERY } from "@/queries/issues-query";
import { MockedProvider, MockedResponse } from "@apollo/client/testing";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import mockRouter from "next-router-mock";
import { MemoryRouterProvider } from "next-router-mock/MemoryRouterProvider";
import Issues from "..";

// eslint-disable-next-line @typescript-eslint/no-unsafe-return
jest.mock("next/router", () => require("next-router-mock"));

const issuesPageOneMock = {
  request: {
    query: ISSUES_QUERY,
    variables: {
      query:
        "mock-query repo:facebook/react type:issue in:title,body state:open sort:updated-desc",
    },
  },
  result: {
    data: {
      search: {
        issueCount: 14,
        pageInfo: {
          hasNextPage: true,
          endCursor: "issues_cursor_1",
          __typename: "PageInfo",
        },
        edges: [
          {
            node: {
              title: "Bug: The placeholder is shown on top of the input value.",
              number: 26327,
              __typename: "Issue",
            },
            __typename: "SearchResultItemEdge",
          },
          {
            node: {
              title:
                "Bug: Constraint Validation API not reflected on Textarea's value attribute change",
              number: 19474,
              __typename: "Issue",
            },
            __typename: "SearchResultItemEdge",
          },
        ],
        __typename: "SearchResultItemConnection",
      },
    },
  },
};

const issuesPageTwoMock = {
  request: {
    query: ISSUES_QUERY,
    variables: {
      cursor: "issues_cursor_1",
      query:
        "mock-query repo:facebook/react type:issue in:title,body state:open sort:updated-desc",
    },
  },
  result: {
    data: {
      search: {
        issueCount: 1,
        pageInfo: {
          hasNextPage: false,
          endCursor: "issues_cursor_2",
          __typename: "PageInfo",
        },
        edges: [
          {
            node: {
              title: "Issue from Page 2.",
              number: 30982,
              __typename: "Issue",
            },
            __typename: "SearchResultItemEdge",
          },
        ],
        __typename: "SearchResultItemConnection",
      },
    },
  },
};

const openCountMock = {
  request: {
    query: ISSUE_COUNT_QUERY,
    variables: {
      query:
        "mock-query repo:facebook/react type:issue in:title,body state:open",
    },
  },
  result: {
    data: {
      search: {
        issueCount: 9,
        pageInfo: {
          hasNextPage: true,
        },
      },
    },
  },
};

const closedCountMock = {
  request: {
    query: ISSUE_COUNT_QUERY,
    variables: {
      query:
        "mock-query repo:facebook/react type:issue in:title,body state:closed",
    },
  },
  result: {
    data: {
      search: {
        issueCount: 5,
        pageInfo: {
          hasNextPage: true,
        },
      },
    },
  },
};

const mocks: MockedResponse[] = [
  issuesPageOneMock,
  issuesPageTwoMock,
  openCountMock,
  closedCountMock,
];

describe("The Issues Page", () => {
  beforeEach(() => {
    mockRouter.query.q = "mock-query";
  });

  describe("when rendered regulary", () => {
    beforeEach(() => {
      render(
        <MemoryRouterProvider>
          <MockedProvider mocks={mocks}>
            <Issues />
          </MockedProvider>
        </MemoryRouterProvider>
      );
    });

    it("should display a spinner while loading", async () => {
      const element = await screen.findByTestId("spinner");
      expect(element).toBeInTheDocument();
    });

    it("should display an error if something went wrong", async () => {
      await screen.findByTestId("spinner");
      const element = await screen.findByText(
        "Bug: The placeholder is shown on top of the input value."
      );
      expect(element).toBeInTheDocument();
    });

    it("should display a button to load more data", async () => {
      await screen.findByTestId("spinner");
      const element = await screen.findByText("Load More");
      expect(element).toBeInTheDocument();
    });

    it("should display a link to filter by open issues", async () => {
      await screen.findByTestId("spinner");
      const element = await screen.findByText("Open (9)");
      expect(element.getAttribute("href")).toEqual("/?q=mock-query&s=open");
    });

    it("should display a link to filter by open issues", async () => {
      await screen.findByTestId("spinner");
      const element = await screen.findByText("Closed (5)");
      expect(element.getAttribute("href")).toEqual("/?q=mock-query&s=closed");
    });

    it("should display a button to load more data", async () => {
      await screen.findByTestId("spinner");
      const element = await screen.findByText("Load More");
      expect(element).toBeInTheDocument();
    });

    it.skip("should request more issues, when the load more button was clicked", async () => {
      await screen.findByTestId("spinner");
      await userEvent.click(await screen.findByText("Load More"));
      const element = await screen.findByText("Issue from Page 2.");
      expect(element).toBeInTheDocument();
    });
  });

  it("should display an error if something went wrong", async () => {
    const networkErrorMock = {
      request: issuesPageOneMock.request,
      error: new Error("Network Error"),
    };

    render(
      <MemoryRouterProvider>
        <MockedProvider
          mocks={[networkErrorMock, openCountMock, closedCountMock]}
          addTypename={false}
        >
          <Issues />
        </MockedProvider>
      </MemoryRouterProvider>
    );

    const element = await screen.findByText("Network Error");
    expect(element).toBeInTheDocument();
  });
});
