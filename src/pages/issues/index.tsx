import Button from "@/components/button";
import IssueList from "@/components/issue-list";
import Message from "@/components/message";
import Spinner from "@/components/spinner";
import StateLink from "@/components/state-link";
import { config } from "@/config";
import { States } from "@/constants";
import useIssueCount from "@/hooks/use-issue-count";
import DefaultLayout from "@/layouts/default-layout";
import { ISSUES_QUERY } from "@/queries/issues-query";
import { useQuery } from "@apollo/client";
import { Issue, SearchResultItemConnection } from "@octokit/graphql-schema";
import { NextPageContext } from "next";
import { useRouter } from "next/router";
import { ParsedUrlQuery } from "querystring";

type Props = {
  initialQuery?: string;
};

function parseQueryParams(query: ParsedUrlQuery) {
  const searchTerm = typeof query.q === "string" ? query.q : "";
  const state =
    typeof query.s === "string" &&
    Object.values(States).includes(query.s as never)
      ? query.s
      : States.OPEN;
  return {
    state,
    searchTerm,
  };
}

export default function Issues({ initialQuery = "" }: Props) {
  const router = useRouter();
  const { state, searchTerm } = parseQueryParams(router.query);

  const baseQuery = [
    searchTerm,
    `repo:${config.repoOwner}/${config.repoName}`,
    "type:issue",
    "in:title,body",
  ].join(" ");

  const issuesSearchQuery = `${baseQuery} state:${state} sort:updated-desc`;
  const closedCountSearchQuery = `${baseQuery} state:closed`;
  const openCountSearchQuery = `${baseQuery} state:open`;

  const {
    loading: loading,
    error: error,
    data: data,
    fetchMore: issuesFetchMore,
  } = useQuery<{
    search: SearchResultItemConnection;
  }>(ISSUES_QUERY, {
    variables: {
      query: issuesSearchQuery,
    },
  });

  const issues = data?.search.edges
    ?.map((edge) => edge?.node)
    .filter(
      (maybeIssue): maybeIssue is Issue => maybeIssue?.__typename === "Issue"
    );

  const closedCount = useIssueCount(closedCountSearchQuery);
  const openCount = useIssueCount(openCountSearchQuery);

  function updateIssues() {
    console.log("fetch more");
    issuesFetchMore({
      variables: {
        cursor: data?.search.pageInfo.endCursor,
      },
    }).catch((error) => {
      // TODO
      console.log(error);
    });
  }

  return (
    <DefaultLayout initialQuery={initialQuery}>
      {loading ? (
        <Spinner className="w-full" />
      ) : error ? (
        <Message type="error">{error.message}</Message>
      ) : issues?.length ? (
        <div className="flex flex-col items-center">
          <div className="flex w-full space-x-8 border-b-4 border-double border-gray-300">
            <StateLink
              active={state === "open"}
              count={openCount}
              label="Open"
              pathname={router.pathname}
              query={router.query}
              state="open"
            />
            <StateLink
              active={state === "closed"}
              count={closedCount}
              label="Closed"
              pathname={router.pathname}
              query={router.query}
              state="closed"
            />
          </div>
          <IssueList className="w-full" issues={issues}></IssueList>
          {data?.search.pageInfo.hasNextPage ? (
            <Button onClick={updateIssues}>Load More</Button>
          ) : null}
        </div>
      ) : (
        <Message type="info">No issues could be found.</Message>
      )}
    </DefaultLayout>
  );
}

// ensure that the q parameter is available on a full page reload
Issues.getInitialProps = (ctx: NextPageContext) => {
  const q = ctx.query.q;
  return {
    initialQuery: typeof q === "string" ? q : "",
  };
};
