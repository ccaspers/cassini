import Button from "@/components/button";
import Comment from "@/components/comment";
import Message from "@/components/message";
import Spinner from "@/components/spinner";
import { config } from "@/config";
import DefaultLayout from "@/layouts/default-layout";
import { ISSUE_QUERY } from "@/queries/issue-query";
import { useQuery } from "@apollo/client";
import { Query } from "@octokit/graphql-schema";
import { useRouter } from "next/router";

type IssueHeaderProps = {
  commentCount?: number;
  state?: string;
  title?: string;
  className?: string;
  issueNumber: number;
};

function IssueHeader({
  commentCount = 0,
  state = "",
  title = "",
  className = "",
  issueNumber,
}: IssueHeaderProps) {
  return (
    <div className={`border-b border-dotted border-gray-400 p-2 ${className}`}>
      <h1 className="text-2xl font-bold">{title}</h1>
      <div className="mt-2 font-mono text-sm">
        #{issueNumber} | State: {state} | {commentCount} Comments
      </div>
    </div>
  );
}

export default function IssueByNumber() {
  const router = useRouter();
  const issueNumber = parseInt(router.query.issueNumber as string, 10);
  const { loading, error, data, fetchMore } = useQuery<Query>(ISSUE_QUERY, {
    variables: {
      issueNumber,
      name: config.repoName,
      owner: config.repoOwner,
    },
  });

  function updateComments() {
    console.log("updating");
    fetchMore({
      variables: {
        cursor: data?.repository?.issue?.comments.pageInfo.endCursor,
      },
    }).catch((error) => {
      // TODO
      console.log(error);
    });
  }

  const hasNextPage = data?.repository?.issue?.comments.pageInfo.hasNextPage;
  const comments = data?.repository?.issue?.comments.nodes || [];

  return (
    <DefaultLayout>
      {error ? (
        <Message type="error">{error.message}</Message>
      ) : loading ? (
        <Spinner className="w-full" />
      ) : (
        <div className="space-y-2">
          <IssueHeader
            className="mb-2 w-full"
            title={data?.repository?.issue?.title}
            state={data?.repository?.issue?.state}
            commentCount={data?.repository?.issue?.comments.totalCount}
            issueNumber={issueNumber}
          />
          <Comment
            html={data?.repository?.issue?.bodyHTML as string}
            author={data?.repository?.issue?.author?.login}
            createdAt={data?.repository?.issue?.createdAt as string}
            lastEditedAt={data?.repository?.issue?.lastEditedAt as string}
          />
          {comments.length ? (
            <div className="flex flex-col items-center space-y-2 px-4">
              {comments.map((comment, index) => (
                <Comment
                  key={index}
                  html={comment?.bodyHTML as string}
                  author={comment?.author?.login}
                  createdAt={comment?.createdAt as string}
                  lastEditedAt={comment?.lastEditedAt as string}
                  className="w-full"
                />
              ))}
              {hasNextPage ? (
                <Button onClick={updateComments}>Load More</Button>
              ) : null}
            </div>
          ) : null}
        </div>
      )}
    </DefaultLayout>
  );
}
