import { useRouter } from "next/router";
import Footer from "../components/footer";
import Header from "../components/header";
import BaseLayout from "./base-layout";

type Props = React.PropsWithChildren<{
  className?: string;
  initialQuery?: string;
}>;

export default function DefaultLayout({ children, initialQuery }: Props) {
  const router = useRouter();
  return (
    <BaseLayout>
      <Header
        className="mb-2"
        initialQuery={initialQuery}
        onSearch={(s) => {
          router
            .push({
              pathname: "/issues",
              query: {
                q: s,
              },
            })
            .catch(() => {
              // ignore errors
            });
        }}
      />
      {children}
      <Footer className="mt-2" />
    </BaseLayout>
  );
}
