type Props = React.PropsWithChildren<{
  centerVertically?: boolean;
  centerHorizontally?: boolean;
  className?: string;
}>;

import localFont from "next/font/local";

const goodBrush = localFont({
  variable: "--font-good-brush",
  src: [
    {
      path: "../fonts/good-brush.woff",
      weight: "400",
      style: "normal",
    },
  ],
});

const iosevka = localFont({
  variable: "--font-iosevka",
  src: [
    {
      path: "../fonts/iosevka/iosevka-extralight.woff2",
      weight: "400",
      style: "regular",
    },
    {
      path: "../fonts/iosevka/iosevka-extralightoblique.woff2",
      weight: "400",
      style: "italic",
    },
    {
      path: "../fonts/iosevka/iosevka-medium.woff2",
      weight: "800",
      style: "regular",
    },
    {
      path: "../fonts/iosevka/iosevka-mediumoblique.woff2",
      weight: "800",
      style: "italic",
    },
  ],
});

export default function BaseLayout({ children, className = "" }: Props) {
  return (
    <main
      className={`${goodBrush.variable} ${iosevka.variable} container flex flex-col py-8 ${className}
        `}
    >
      {children}
    </main>
  );
}
