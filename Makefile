SOURCE_FILES:=$(shell find src -type f)

all: lint test dist

#-----------------------------------------------------------
# file based targets
#-----------------------------------------------------------

package-lock.json: package.json
	npm i --package-lock-only

node_modules: package-lock.json
	npm ci

dist: node_modules $(SOURCE_FILES) next.config.js
	npm run build

#-----------------------------------------------------------
# handy commands
#-----------------------------------------------------------

dev: node_modules
	npm run dev

lint: node_modules
	npm run lint

test: node_modules
	npm run test

build: dist

clean:
	git clean -Xfd

.PHONY: dev lint test build clean