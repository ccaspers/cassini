# TODO

Goal: Build a frontend for the GitHub GraphQL API. It should allow to browse the issues of the official React repository.

## Tasks

- [x] home view with search input field `/`
- [ ] search results view on `/issues?q=${term}&status=${status}&page=${pageNumber}`
  - [x] `term` matches title or body
  - [ ] `status` is either `OPEN` or `CLOSED`, optional
  - [ ] `pageNumber` positive integer, 1-index based (page in url should be equal to page
        displayed in pager)
  - pagination
    - current page +- 2
    - first page
    - last page
    - arrows to next, prev
    - (<)(1)...(5)(6)(**7**)(8)(9)...(99)(>)
- [] issues on `/issue/${issuedId}`
  - issue with all comments
- [] search visible on all views
- [] link to `/` on header on all views
- [] prepare presentation

# Optional

- [] automated deployment to github pages
- [x] search in react repo, but make it configurable

## Constraints

- Typescript
- ES6+ features (async/await)
- React
- Redux
- caching
- tests
  - junit tests with jest
  - some cypress e2e tests
