# Cassini

A toy application built using nextjs that can query github issues.

## Getting Started

First, run the development server:

```bash
make dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## About The Name

Cassini-Huygens was a space probe that visited Saturn. Huygens landed
on Titan, while Cassini orbited Saturn to study it.
